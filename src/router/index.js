import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
// import Register from "../views/Register.vue";
// import UserDashboard from "../views/user/Dashboard.vue";
// import UserJoined from "../views/user/Joined.vue";
// import OrganizationDashboard from "../views/organization/Dashboard.vue";
// import OrganizationCreate from "../views/organization/Create.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/register",
    name: "Register",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Register.vue")
  },
  {
    path: "/user/dashboard",
    name: "UserDashboard",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/user/Dashboard.vue")
  },
  {
    path: "/user/joined",
    name: "UserJoined",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/user/Joined.vue")
  },
  {
    path: "/organization/dashboard",
    name: "OrganizationDashboard",
    component: () =>
      import(
        /* webpackChunkName: "about" */ "../views/organization/Dashboard.vue"
      )
  },
  {
    path: "/organization/create",
    name: "OrganizationCreate",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/organization/Create.vue")
  }
];

const router = new VueRouter({
  mode: "history", // Add this line
  routes
});

export default router;
