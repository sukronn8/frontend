import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:3005/api/";

class UserService {
  // getPublicContent() {
  //   return axios.get(API_URL + "all");
  // }
  getUserBoard() {
    return axios.get(API_URL + "attend", { headers: authHeader() });
  }

  getOrganizationBoard() {
    return axios.get(API_URL + "event", { headers: authHeader() });
  }
}

export default new UserService();
