export default class User {
  constructor(username, email, password, phone, id_roles) {
    this.username = username;
    this.email = email;
    this.password = password;
    this.phone = phone;
    this.id_roles = id_roles;
  }
}
