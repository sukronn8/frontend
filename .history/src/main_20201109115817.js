import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Store from "./store";
import "@/assets/stylesheets/main.css";
import VeeValidate from "vee-validate";

Vue.config.productionTip = false;

Vue.use(VeeValidate);

new Vue({
    router,
    store: Store,
    render: h => h(App)
}).$mount("#app");