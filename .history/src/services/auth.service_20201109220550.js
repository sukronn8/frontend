import axios from "axios";

const API_URL = "http://localhost:3005/api/";

class AuthService {
    login(user) {
        return axios
            .post(API_URL + "login", {
                email: user.email,
                password: user.password
            })
            .then(this.handleResponse)
            .then(response => {
                //eslint-disable-next-line
                debugger;
                if (response.data.accessToken) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                    return response.data;
                }
            });
    }

    logout() {
        localStorage.removeItem("user");
    }

    register(user) {
        return axios.post(API_URL + "register", {
            name: user.name,
            email: user.email,
            password: user.password,
            phone: user.phone,
            id_roles: user.id_roles
        });
    }

    handleResponse(response) {
        if (response.status === 401) {
            this.logout();
            location.reload(true);

            const error = response.data && response.data.message;
            return Promise.reject(error);
        }

        return Promise.resolve(response);
    }
}

export default new AuthService();